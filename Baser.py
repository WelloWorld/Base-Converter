#!/usr/bin/python2
#import ScrolledText
import Tkinter as tk
from hashlib import md5

def is_int(var):
	return var.isdigit()
def is_hex(var):
	return (var[0] is '0' and (var[1] is 'x') or (var[1] is 'X'))
def is_str(var):
	return (var[0] is "'" and var[-1] is "'")
def is_bin(var):
	return (var[0] is '0' and (var[1] is 'b') or (var[1] is 'B'))

class SampleApp(tk.Tk):
	def __init__(self):

		tk.Tk.__init__(self)
		t = tk.Text(self,height=5)
		t.insert(tk.INSERT,'Int: <number>\nHex: 0x<hex>\nBin: 0b<binary>\nString: \'<string>\'')
		t.pack()
		self.entry = tk.Entry(self,textvariable=20)
		#self.entry = tk.ScrolledText.ScrolledText(self, wrap=tk.WORD)
		self.button = tk.Button(self, text="Get", command=self.on_button,height = 2, width = 70)
		self.anotherButton = tk.Button(self,text='Exit',command=self.exitme,height = 2, width = 70)
		self.anotherButton.pack()
		self.button.pack()
		self.entry.pack()

	def exitme(self):
		self.destroy()
		self.quit()
	
	def on_button(self): 
		ls= []
		data = self.entry.get()
		self.n = tk.Tk()
		self.t = tk.Text(self.n)
		s=0
		st=''
		base =0
		var_is_string=False

		if is_str(data):
			var_is_string=True
		else:	
			if is_hex(data): #hex
				base = 16

			elif is_int(data): #iint
				base = 10

			elif is_bin(data):#bin
				base=2

			else:
				self.exitme()	
		
		if(not var_is_string):		#currently not support values like '101 453 180' as ints
			try:
				s = int(data,base)
				ls.append('---------------------------------------\nInt: '+str(s)) ######     string to int 
				ls.append('---------------------------------------\nHex: '+str(hex(s))) ######     string to hex 
				ls.append('---------------------------------------\nBinary: '+str(bin(s))[2:]) ######     string to binary 
				ls.append('---------------------------------------\nBASE-64: '+str(s).encode('base64').replace('\n',''))
				ls.append('---------------------------------------\nMD5: ' + str(md5(str(s)).hexdigest()))
			except Exception, e:
				print str(e)	
				self.exitme()
		else: #For strings
			st = data[1:-1] #remove ''
			ints, hexs, bins=[],[],[]

			for char in st:
				ord_value = ord(char)
				ints.append(str(ord_value)+' ')
				hexs.append(str(hex(ord_value))+' ')
				bins.append(str(bin(ord_value))[2:]+' ')

			ls.append('---------------------------------------\nString: '+data)
			ls.append('---------------------------------------\nInt: '+''.join(ints))
			ls.append('---------------------------------------\nHex: '+''.join(hexs))
			ls.append('---------------------------------------\nBinary: '+''.join(bins))
			ls.append('---------------------------------------\nBASE-64: '+str(st.encode('base64')).replace('\n',''))
			ls.append('---------------------------------------\nMD5: ' + str(md5(st).hexdigest()))



		for x in ls:
			self.t.insert(tk.END, x + '\n')
		self.t.pack()
		self.n.mainloop()

app = SampleApp()
app.geometry("400x400")
app.mainloop()
